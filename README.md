# LottCraft Release Engineering

## Plugin Classes

Plugins are organized into three classes

1. **Core**
2. **Main**
3. **Extra**

Plugin classes are used to prioritize the importance of plugins to the successful operation of the server.

## Class listings

Below is the listing of plugins in each class. Plugins may switch classes at any time in accordance with shifts in their importance to the server.

#### Core

- Essentials
- EssentialsProtect
- Multiverse-Core
- PermissionsBukkit

#### Administrative

- Clearlag
- MobileAdmin
- Plugman
- ScheduledAnnouncer

#### Gameplay

- Autosort
- Dynmap
- EssentialsSpawn
- ServerSigns
- Vault

#### Extra

- Dynmap-Essentials
- EssentialsChat
- EssentialsGeoIP
- EssentialsXMPP

## Stabilizing Releases

The stabilization path for each release will go as follows:

Plugins will be tested in order for each release, starting with Core ending with Extra. When a plugin under testing is determined to work with the current bukkit release, the plugin is stabilized. When all plugins in the current class are stabilized, the release changes stability phases. Stability phases are determined as follows.

- **.Pre-Release**: Core plugins are not yet stabilized
- **-Development**: Core plugins have been stabilized
- **~Unstable**: Core and Administrative plugins have been stabilized
- **Stable**: Core, Administrative, and Gameplay plugins have been stabilized
- **+Mature**: All plugins have been stabilized

## Other Documents

- **VERSION.md**: This document will maintain information on the current Bukkit version, Minecraft version, and stability status of the release.
