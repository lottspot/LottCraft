# Release Information

### Bukkit Version

CraftBukkit: 1.7.2-R0.2

### Plugin Versions

**Emboldened** plugins have been stabilized on this Bukkit release.

**EssentialsXMPP**: TeamCity  
**EssentialsGeoIP**: TeamCity  
**EssentialsSpawn**: TeamCity  
**dynmap**: 1.9.1-875  
**ServerSigns**: 3.0.0  
**Essentials**: TeamCity  
**Clearlag**: 2.6.2  
**Dynmap-Essentials**: 0.70  
**PlugMan**: 1.8.4  
**AutoSort**: 0.8.5  
**EssentialsProtect**: TeamCity  
**Multiverse-Core**: 2.4-b527  
**PermissionsBukkit**: 2.0  
**Vault**: 1.2.31-b411  
**ScheduledAnnouncer**: 2.8.3  
**EssentialsChat**: TeamCity  
**MobileAdmin**: 3.2.5  
