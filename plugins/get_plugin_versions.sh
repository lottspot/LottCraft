#!/bin/bash

for p in $(find -type l -name '*.jar');
do
  file=$(echo $p | awk -F/ '{print $NF}')
  name=$(echo $file | perl -ne 'my $out=substr($_, 0, -5); print $out')
  version=$(unzip -p $p plugin.yml | egrep '^version: ' | cut -d' ' -f2)
  echo "$name: $version  "
done
